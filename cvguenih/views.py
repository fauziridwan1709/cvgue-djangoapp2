from django.shortcuts import render
from django.http import HttpResponse

def home(request):
    return render(request, 'cvguenih/home.html')

def contact(request):
    return render(request, 'cvguenih/contact.html')

def project(request):
    return render(request, 'cvguenih/index.html')